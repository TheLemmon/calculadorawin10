﻿Class MainWindow

    Private expresion As String = ""
    ' Private strNum As String = ""
    Private cadena As String = ""

    Private Sub Button_Click(sender As Object, e As RoutedEventArgs)
        cadena = CType(sender, Button).Content
        If Not (cadena.Equals("=") Or cadena.Equals("C") Or cadena.Equals("CE") Or cadena.Equals("⌫") Or
            cadena.Equals("±")) Then
            expresion = expresion + cadena
        End If

        Select Case (CType(sender, Button).Content)
            Case "M"
            Case "^"
                Historial.Text = expresion
                Resultado.Text = ""
            Case "CE"
                Resultado.Text = ""
             ' strNum = ""
            Case "C"
                Resultado.Text = ""
                expresion = ""
                Historial.Text = ""
            Case "⌫"

                If (Resultado.Text.Length - 1 >= 0) Then
                    Resultado.Text = Resultado.Text.Remove(Resultado.Text.Length - 1)
                    expresion = expresion.Remove(expresion.Length - 1)
                End If

            Case "/"
                Historial.Text = expresion
                Resultado.Text = ""
            'Historial.Text = strNum
            Case "±"
                If (Resultado.Text.Substring(0, 1) <> "-") Then
                    expresion = expresion.Replace(Resultado.Text, "-" & Resultado.Text)
                    Resultado.Text = "-" + Resultado.Text
                Else
                    expresion = expresion.Replace(Resultado.Text, Resultado.Text.Substring(1))
                    ' MsgBox(Resultado.Text.Substring(1))
                    Resultado.Text = Resultado.Text.Substring(1)
                End If
            Case "+"
                Historial.Text = expresion
                Resultado.Text = ""
                'Historial.Text = strNum
            Case "-"
                Historial.Text = expresion
                Resultado.Text = ""
                'Historial.Text = strNum
            Case "*"
                Historial.Text = expresion
                Resultado.Text = ""
                'Historial.Text = strNum
            Case "1"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "2"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "3"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "4"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "5"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "6"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "7"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "8"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "9"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "0"
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "."
                Resultado.Text = Resultado.Text + (CType(sender, Button).Content)
            Case "="
                Resultado.Text = Main(expresion)
                Historial.Text = ""
                expresion = Resultado.Text
        End Select
    End Sub

    Function Main(ByVal number As String) As String
        Dim value As String = number
        Dim expresion As String
        Dim newExpresoin As String
        Dim n1 As Integer = 0
        Dim min As Integer = 0
        Dim num1 As Integer = 0
        Dim num2 As Integer = 0
        value = value.Replace(" "c, String.Empty)
        value = value.Replace("+", " + ").Replace("*", " * ").Replace(".", ",")
        value = value.Replace("/", " / ").Replace("(", " ( ")
        value = value.Replace(")", " ) ").Replace("-", " - ")
        value = value.Replace("^", " ^ ")
        value = " " & value
        value = value.Replace("  ", " ")
        Dim allValues As String() = value.Split(New [Char]() {" "c, CChar(vbTab)})

        For Each count As String In value
            If (count = "+" Or count = "*" Or count = "/" Or count = "-" Or count = "^") Then
                n1 += 1
            End If
        Next

        For Each count As String In value
            If (count = "^") Then
                num1 += 1
            End If
            If (count = "*" Or count = "/") Then
                num2 += 1
            End If
        Next

        For count As Integer = 0 To n1
            Try
                For count2 As Integer = 0 To allValues.Length - 1
                    allValues = value.Split(New [Char]() {" "c, CChar(vbTab)})
                    If (allValues(count2) = "^") Then
                        num1 -= 1
                        If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("^", " ^ ").Replace(" -", " - "), Expo(expresion))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("^", " ^ "), " " & Expo(expresion).Replace("-", "- "))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("^", " ^ ").Replace("  ", " "), Expo(expresion).Replace("-", " - "))
                        End If
                        If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("^", " ^ "), Expo(expresion))
                        End If

                    ElseIf (allValues(count2) = "*" And num1 <= 0) Then
                        num2 -= 1
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * "), Mult(expresion).Replace("-", " - "))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * ").Replace("  ", " "), " " & Mult(expresion))
                        End If
                        If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("*", " * ").Replace("  ", " "), Mult(expresion).Replace("-", "- "))
                        End If
                        If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("*", " * "), Mult(expresion))
                        End If

                    ElseIf (allValues(count2) = "/" And num1 <= 0) Then
                        num2 -= 1
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / "), Div(expresion).Replace("-", " - "))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / ").Replace("  ", " "), " " & Div(expresion))
                        End If
                        If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("/", " / ").Replace("  ", " "), Div(expresion).Replace("-", "- "))
                        End If
                        If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("/", " / "), Div(expresion))
                        End If

                    ElseIf (allValues(count2) = "+" And num2 <= 0 And num1 <= 0) Then
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + "), Sum(expresion).Replace("-", " - "))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), " " & Sum(expresion))
                        End If
                        If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), Sum(expresion).Replace("-", "- "))
                        End If
                        If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("+", " + "), Sum(expresion))
                        End If

                    ElseIf (allValues(count2) = "-" And num2 <= 0 And num1 <= 0 And count2 > 1) Then
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) <> "-") Then
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + "), Mini(expresion).Replace("-", " - "))
                        End If
                        If (allValues(count2 - 2) = "-" And allValues(count2 + 1) = "-") Then
                            newExpresoin = allValues(count2 - 2) & allValues(count2 - 1) & "+" & allValues(count2 + 2)
                            expresion = allValues(count2 - 2) & allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("  ", " "), Sum(newExpresoin).Replace("-", "- "))
                        End If
                        If (allValues(count2 + 1) = "-" And allValues(count2 - 2) <> "-") Then
                            newExpresoin = allValues(count2 - 1) & "+" & allValues(count2 + 2)
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1) & allValues(count2 + 2)
                            value = value.Replace(expresion.Replace("-", " - ").Replace("+", " + ").Replace("  ", " "), Sum(newExpresoin).Replace("-", "- "))
                        End If
                        If (allValues(count2 + 1) <> "-" And allValues(count2 - 2) <> "-") Then
                            expresion = allValues(count2 - 1) & allValues(count2) & allValues(count2 + 1)
                            value = value.Replace(expresion.Replace("-", " - "), Mini(expresion))
                        End If
                    End If
                Next
            Catch ex As Exception

            End Try
        Next
        Try
            Dim result As Double = Convert.ToString(value.Replace(" ", String.Empty))
        Catch ex As Exception
            value = "Error"
        End Try
        Return value.Replace(" ", String.Empty)

    End Function

    Function Sum(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double
        Dim n1 As String
        Dim num2 As Double
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("+") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 + num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Mult(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("*") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 * num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Div(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("/") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                If (num2 = 0) Then

                End If
                num2 = num1 / num2
                result = Convert.ToString(num2)
            End If
        Next

        Return result
    End Function

    Function Expo(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("^") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 ^ num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

    Function Mini(ByVal value As String) As String
        Dim result As String
        Dim num1 As Double = 0
        Dim n1 As String
        Dim num2 As Double = 0
        Dim n2 As String
        Dim count As Double = 0

        For Each valueOne As String In value
            count = count + 1
            If (valueOne.Equals("-") And count > 1) Then
                n1 = value.Substring(0, count - 1)
                num1 = Double.Parse(n1)
                n2 = value.Substring(count)
                num2 = Double.Parse(n2)
                num2 = num1 - num2
                result = Convert.ToString(num2)
            End If
        Next
        Return result
    End Function

End Class
